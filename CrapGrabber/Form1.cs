﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace CrapGrabber
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        public List<DriveInfo> removableDrives = new List<DriveInfo>();



        /// <summary>
        /// Get a list of all removable devices
        /// </summary>
        /// <returns></returns>
        private List<DriveInfo> getAllUsbDrives ()
        {

            List<DriveInfo> list = new List<DriveInfo>();

            // foreach drives
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {

                // is a removable drive
                if (drive.DriveType == DriveType.Removable)
                {
                    list.Add(drive);
                }

            }

            return list;

        }





        /// <summary>
        /// Create a new drive control
        /// </summary>
        /// <param name="drive"></param>
        private void createDriveControl (DriveInfo drive)
        {

            driveLayout.Controls.Add(new CheckBox() { Text = drive.Name, Name="drive_" + driveLayout.RowCount.ToString(), Checked=true }, 0, driveLayout.RowCount-1 );
            driveLayout.Controls.Add(new TextBox() { Name = "path_" + driveLayout.RowCount, Text = drive.VolumeLabel, ReadOnly=true}, 1, driveLayout.RowCount - 1);
            driveLayout.Controls.Add(new TextBox() { Name = "student_" + driveLayout.RowCount, Text = "Eleve"+ driveLayout.RowCount.ToString() }, 2, driveLayout.RowCount-1);

            driveLayout.Controls.Add(new ProgressBar() { Name = "progress_" + driveLayout.RowCount, Value = 0 }, 3, driveLayout.RowCount - 1);

            driveLayout.RowCount++;
        }



        /// <summary>
        /// Draw all drives on form
        /// </summary>
        private void drawAllDrives ()
        {
            // reset row count
            driveLayout.Controls.Clear();
            driveLayout.RowCount = 0;

            // get all removable drives
            removableDrives = getAllUsbDrives();


            // there is at least one removable drive
            if (removableDrives.Count > 0)
            {

                foreach (DriveInfo drive in removableDrives)
                {
                     // create a new control
                    createDriveControl(drive);
                }

            }
            else
            {
                MessageBox.Show("no removable drive found !");
            }
        }





        /// <summary>
        /// on form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {

            // show all drives on form
            drawAllDrives();
            
        }




        /// <summary>
        /// grab the file from a drive and copy to the destination
        /// </summary>
        /// <param name="destinationFolder"></param>
        /// <param name="drive"></param>
        private void grabFiles (string destinationFolder, string drive, ProgressBar pb)
        {
            
            // get all files in folder
            var files = Directory.EnumerateFiles(drive, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".mp3", StringComparison.OrdinalIgnoreCase) || s.EndsWith(".wav", StringComparison.OrdinalIgnoreCase));

            // Progress bar init (a separate thread can't access the ui thread controls so --> invoke)
            Invoke(new Action( ()=> { pb.Value = 0;  pb.Maximum = files.Count(); } ));

            // copy files
            foreach (string file in files)
            {
                Console.WriteLine("Copy : " + file);

                // file do not exists or overwrite
                if (!File.Exists(destinationFolder + "\\" + Path.GetFileName(file)) || overwiteDuplicate.CheckState == CheckState.Checked)
                {
                    File.Copy(file, destinationFolder + "\\" + Path.GetFileName(file), true);
                }
                
                // file exists and do not overwrite
                else
                {
                    string filename = Path.GetFileNameWithoutExtension(file);
                    string ext = Path.GetExtension(file);

                    Random r = new Random();
                    int rInt = r.Next(0, 10000);

                    string newFileName = filename + "_" + rInt.ToString() + ext;

                    try
                    {
                        File.Copy(file, destinationFolder + "\\" + newFileName);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error copying : " + ex.Message);
                    }
                    
                }


                // delete the file
                if (deleteSource.CheckState == CheckState.Checked)
                {
                    File.Delete(file);
                }

                // make the progress bar bigger !
                Invoke(new Action(() => { pb.Value++; }));
                

            }
        }



        /// <summary>
        /// refresh the drives list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rafraichirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drawAllDrives();
        }





        /// <summary>
        /// Go and download the files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void goToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // show the folder dialog
            DialogResult result = folderBrowser.ShowDialog();


            // OK was pressed
            if (result == DialogResult.OK)
            {

                Console.WriteLine("I must grab files and copy here : " + folderBrowser.SelectedPath);


                // make a root folder 
                if (!Directory.Exists(folderBrowser.SelectedPath + "\\craps"))
                {
                    Directory.CreateDirectory(folderBrowser.SelectedPath + "\\craps");
                }



                // foreach drives
                for (int index = 0; index < driveLayout.RowCount; index++)
                {

                    // get checkbox state
                    CheckBox chk = driveLayout.Controls["drive_" + index] as CheckBox;

                    // get the student
                    TextBox student = driveLayout.Controls["student_" + index] as TextBox;

                    // get the progressbar
                    ProgressBar pb = driveLayout.Controls["progress_" + index] as ProgressBar;

                    // the checkbox is checked --> go fw
                    if (chk.CheckState == CheckState.Checked)
                    {
                        string drive = removableDrives[index].Name;
                        string label = removableDrives[index].VolumeLabel;
                        string studentName = student.Text;




                        // make the student folder
                        if (!Directory.Exists(folderBrowser.SelectedPath + "\\craps\\" + studentName))
                        {
                            Directory.CreateDirectory(folderBrowser.SelectedPath + "\\craps\\" + studentName);
                        }


                        // do the job in a new thread
                        Thread thread = new Thread(() => grabFiles(folderBrowser.SelectedPath + "\\craps\\" + studentName, drive, pb));
                        thread.Start();

                    }


                }


            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Permet de récupérer tous les fichiers mp3 et wav de lecteurs amovibles en batterie. (par dividi sur gitlab)", "A propos", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }


    } // end o class
}// end o namespace
