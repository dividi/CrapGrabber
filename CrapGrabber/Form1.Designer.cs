﻿namespace CrapGrabber
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.driveLayout = new System.Windows.Forms.TableLayoutPanel();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSource = new System.Windows.Forms.ToolStripMenuItem();
            this.overwiteDuplicate = new System.Windows.Forms.ToolStripMenuItem();
            this.rafraichirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // driveLayout
            // 
            this.driveLayout.AutoSize = true;
            this.driveLayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.driveLayout.ColumnCount = 4;
            this.driveLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.driveLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.driveLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 204F));
            this.driveLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.driveLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this.driveLayout.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.driveLayout.Location = new System.Drawing.Point(0, 29);
            this.driveLayout.Name = "driveLayout";
            this.driveLayout.RowCount = 1;
            this.driveLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.driveLayout.Size = new System.Drawing.Size(439, 2);
            this.driveLayout.TabIndex = 0;
            // 
            // folderBrowser
            // 
            this.folderBrowser.Description = "Where to grab da crap";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.rafraichirToolStripMenuItem,
            this.goToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(439, 29);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.optionsToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSource,
            this.overwiteDuplicate});
            this.optionsToolStripMenuItem.Image = global::CrapGrabber.Properties.Resources.ic_build_black_18dp;
            this.optionsToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(93, 25);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.ToolTipText = "Les réglages";
            // 
            // deleteSource
            // 
            this.deleteSource.CheckOnClick = true;
            this.deleteSource.Name = "deleteSource";
            this.deleteSource.Size = new System.Drawing.Size(231, 26);
            this.deleteSource.Text = "Supprimer les fichiers";
            this.deleteSource.ToolTipText = "Supprimer les fichiers après copie";
            // 
            // overwiteDuplicate
            // 
            this.overwiteDuplicate.Checked = true;
            this.overwiteDuplicate.CheckOnClick = true;
            this.overwiteDuplicate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.overwiteDuplicate.Name = "overwiteDuplicate";
            this.overwiteDuplicate.Size = new System.Drawing.Size(231, 26);
            this.overwiteDuplicate.Text = "Ecraser les doublons";
            this.overwiteDuplicate.ToolTipText = "Ecraser les fichiers en doublon (ou les renommer)";
            // 
            // rafraichirToolStripMenuItem
            // 
            this.rafraichirToolStripMenuItem.Image = global::CrapGrabber.Properties.Resources.ic_cached_black_18dp;
            this.rafraichirToolStripMenuItem.Name = "rafraichirToolStripMenuItem";
            this.rafraichirToolStripMenuItem.Size = new System.Drawing.Size(105, 25);
            this.rafraichirToolStripMenuItem.Text = "Rafraichir";
            this.rafraichirToolStripMenuItem.ToolTipText = "Rafraichir la liste des lecteurs";
            this.rafraichirToolStripMenuItem.Click += new System.EventHandler(this.rafraichirToolStripMenuItem_Click);
            // 
            // goToolStripMenuItem
            // 
            this.goToolStripMenuItem.Image = global::CrapGrabber.Properties.Resources.ic_check_circle_black_18dp;
            this.goToolStripMenuItem.Name = "goToolStripMenuItem";
            this.goToolStripMenuItem.Size = new System.Drawing.Size(58, 25);
            this.goToolStripMenuItem.Text = "Go";
            this.goToolStripMenuItem.ToolTipText = "Lancer la copie des fichiers";
            this.goToolStripMenuItem.Click += new System.EventHandler(this.goToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(29, 25);
            this.toolStripMenuItem1.Text = "?";
            this.toolStripMenuItem1.ToolTipText = "Quelques informations";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 320);
            this.Controls.Add(this.driveLayout);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Crap Grabber";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel driveLayout;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSource;
        private System.Windows.Forms.ToolStripMenuItem overwiteDuplicate;
        private System.Windows.Forms.ToolStripMenuItem rafraichirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

